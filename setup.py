import os
from setuptools import setup


with open("README.md", "r") as readme:
    readme_file = readme.read()

if os.environ.get("CI_COMMIT_TAG"):
    version = os.environ.get("CI_COMMIT_TAG")
else:
    version = os.environ.get("CI_JOB_ID")


setup(
    name="devops-py",
    version=version,
    author="Engel J. Pinto",
    author_email="engeljavierpinto@gmail.com",
    description="Una prueba de distribución con GITLAB",
    long_description=readme_file,
    packages=["devops"],
    install_requires="requirements.txt",
    url="{0}".format(os.environ.get("CI_PROJECT_URL")),
    zip_safe=False
)
